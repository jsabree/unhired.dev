import "./styles.css";
import unhired from "./assets/unhired.webp";

const App = () => {
  return (
    <div className="container">
      <header>
        <h1 className="subtitle">Jawwaad Sabree - Full Stack Developer</h1>
      </header>
      <main>
        <img className="image" src={unhired} alt="Profile" />
        <div className="experience">
          <p>2022 - Present | Senior React Developer @Showseeker</p>
          <p>2021 - 2022 | React Native Developer @Parknfly</p>
          <p>2020 - 2021 | Contractor @Rower</p>
          <p>2019 - 2020 | Software Engineer @Central State University</p>
        </div>
      </main>
    </div>
  );
};

export default App;
